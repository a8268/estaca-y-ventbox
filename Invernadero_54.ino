



////////////

//#define ESTACA
#define ESTACA
// ESTACA y/o VENTBOX


#ifdef VENTBOX
//////////////////////////////////////////////////////////////// LCD
#include <Wire.h>
#include <LiquidCrystal_I2C.h> //Marco Schwartz
LiquidCrystal_I2C lcd(0x27, 16, 2); // set the LCD address to 0x27 for a 16 chars and 2 line display
#endif


String DEVICE_NAME = "ESTACA047";

#define DHT_PIN D1 // pin D2 /////// GPIO 2
#define ONE_WIRE_BUS  D7

#define rele_one D6

// 18B20 SENSOR ONE WIRE  PIN D7
#define sensorPin   A0 // A0 ANALOGICO

// Led en PCB
int LED_DELAY = 1000;

#define LED_BUILTIN D2


/////////////////////////////////////////////////////////////////////////////////////////////////////////////// WIFI
#define SECRET_SSID "MiFibra-5383"    // replace MySSID with your WiFi network name
#define SECRET_PASS "yUPupCtg"  // replace MyPassword with your WiFi password
#include <ESP8266WiFi.h>


boolean enMosquitto = true;

// 2 Módulo extractores

///////////////////////////////////////////////////////////////////////////////////////////////////////// MQTT

///////////////////////////////////////////////////////////////////////////////////////////// thingspeak
#include "ThingSpeak.h"
#define SECRET_CH_ID 1652188
#define SECRET_WRITE_APIKEY "5Z26ORS6IEMLJ2MO"   // replace XYZ with your channel write API Key

unsigned long myChannelNumber = SECRET_CH_ID;
const char * myWriteAPIKey = SECRET_WRITE_APIKEY;

// Initialize our values
int number1 = 0;
int number2 = random(0, 100);
int number3 = random(0, 100);
int number4 = random(0, 100);
String myStatus = "";
int MAX_TEMP = 30; ///

//Variables para mensajes en las dos lineas del lcd
String lcd_linea1, lcd_linea2;

////////////////////////////

//Controlar el parpadeo del SEGÚN TEMPERATURA 18B20

#define LED_PARPADEO_MAX 2500
#define LED_PARPADEO_MIN 100
#define LED_PARPADEO_TEMP_MAX 35
#define LED_PARPADEO_TEMP_MIN 25


int ONLINE_TEMP_CONTROL;
///////////////////////////////////////////////////////////////////////////////////////////// mosquitto
#include <PubSubClient.h>


char ssid[] = SECRET_SSID;   // your network SSID (name)
char pass[] = SECRET_PASS;   // your network password
int keyIndex = 0;            // your network key Index number (needed only for WEP)
WiFiClient client;


////////////////////////////////////////////////////////////// Mosquitto
int number = 0;
const char* mqtt_server = "192.168.1.131";
PubSubClient mqtt_sub_pub_client(client);
#define MSG_BUFFER_SIZE (200)
char msg[MSG_BUFFER_SIZE];


////////////////////////////////////////////////////////////// SENSOR DHT11
#include "DHT.h"
#define DHTTYPE DHT11   // DHT 11
// Uncomment whatever type you're using!
//#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321
//#define DHTTYPE DHT21   // DHT 21 (AM2301)
DHT dht(DHT_PIN, DHTTYPE);



////////////////////////////////////////////////////////////// ONE WIRE + SENSOR DALLAS 18B20
#include <OneWire.h>
#include <DallasTemperature.h>
OneWire oneWire(ONE_WIRE_BUS); // Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
DallasTemperature sensors(&oneWire); // Pass our oneWire reference to Dallas Temperature.
DeviceAddress insideThermometer; // arrays to hold device address









void setup()
{
  Serial.begin(115200);

  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(rele_one, OUTPUT);

  delay (500);

  Bienvenida();



  setup_wifi();

  Serial.println("\n MIKEL TEMPERATURA 39\n");

#ifdef VENTBOX
  enviaAlerta ("VENTBOX ACTIVADO *");
  enMosquitto = false;
#endif

#ifdef ESTACA
  enviaAlerta("ESTACA ACTIVADA *");
  Serial.println("Software de estaca");

#endif


  ///////////////////////////////// PARTE 1 ONEWIRE
  enviaAlerta ("Inicializando 18B20 *");
  initDallas();

  ///////////////////////////////// PARTE 2 DHT
  enviaAlerta("Inicializando DHT *");
  dht.begin();

  Serial.println();

  // MQTT Mosquitto
  enviaAlerta ("Inicializando Cliente MQTT");
  mqtt_sub_pub_client.setServer(mqtt_server, 1883);

  /////////////////////////////////////////////////////////// THINGSPEAK
  ThingSpeak.begin(client);  // Initialize ThingSpeak



  digitalWrite(LED_BUILTIN, HIGH);


  enviaAlerta("**DISPOSITIVO " + DEVICE_NAME + " ENCENDIDO *");



}



unsigned long millisThingspeak, millisLeerTemperatura;

float hdty1, temp1, temp2;

float valorLdr;


void loop()
{

  // digitalWrite(LED_BUILTIN, LOW);   // turn the LED ON

  unsigned long currentMillis = millis(); // Millis devuelve los milisegundos que hace desde que se encendió el microcontrolador.

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  if (currentMillis > millisLeerTemperatura + 1000) ///////////////////////////////////////////////////////////// TIMER 1: LEE SENSORES DE TEMPERATURA, HUMEDAD Y LDR
  {
    millisLeerTemperatura = currentMillis;

#ifdef ESTACA
    /////////////////////////////////////////////////////////////////////// PARTE 1 SENSOR DHT /////////////
    hdty1 = dht.readHumidity();
    temp1 = dht.readTemperature();
    if (isnan(hdty1) || isnan(temp1)) {
      Serial.println("Failed to read from DHT sensor!");
      hdty1 = 50; temp1 = 50;
    }

    else
    {
      Serial.print("\tHumidity: ");    Serial.print(hdty1);
      Serial.print("\t");    Serial.print("TEMP1: ");
      Serial.print(temp1);    Serial.print(" *C ");
    }
#endif

    /////////////////////////////////////////////////////////////////////////// PARTE 2 SENSOR ONEWIRE ////////
    temp2 = calcularTemperature(insideThermometer);
    Serial.print("\tTEMP2: ");
    Serial.print(temp2); // Use a simple function to print out the data


#ifdef ESTACA
    /////////////////////////////////////////////////////////////////////////// PARTE 3 SENSOR LDR
    float sensorValue = analogRead(sensorPin);
    Serial.print(sensorValue);
    //regla de 3
    // 1 --- 0v
    // 1024 --- 3.3v
    valorLdr = map (sensorValue, 1, 1024, 0, 100);
    Serial.print (", valorLdr= " + String(valorLdr) + "%");
#endif

    Serial.println("");

  }






  //////////////////////////////////////////////////////////////////////////////////////////////////////////////// TIMER 2
  static unsigned long timerLocalMqtt;
  if (currentMillis > timerLocalMqtt + 5000 && enMosquitto)
  {
    timerLocalMqtt = currentMillis;

    /////////////////////////////////////////////////////////////////////////////////////////// MOSQUITTO /////
    if (!mqtt_sub_pub_client.connected()) {
      Serial.println("DESCONECTADO MOSQUITTO");
      ///////////////////////////////////////////////////////////////////////
      Serial.print("Conectando a: ");
      Serial.println(mqtt_server);
      reconnect_mqtt(3, 1000); //
    }
    else
    {
      Serial.println("CONECTADO MOSQUITTO > Enviando mensaje");
      mqtt_sub_pub_client.loop();

      char* buffn = "";
      dtostrf(hdty1, 5, 2, buffn);
      char* formato = "{\"name\":\"humidity_sensor\",\"value\": %s}";

      sprintf(msg, formato, buffn);
      Serial.print("\nPublish message: ");       Serial.println(msg);
      mqtt_sub_pub_client.publish("sensors", msg); // Impacta

      //////////////////////////////////////////////////////////////////////////

      mqtt_sub_pub_client.loop();

      buffn = "";
      dtostrf(temp2, 5, 2, buffn);
      formato = "{\"name\":\"temperatura1_sensor\",\"value\": %s}";

      sprintf(msg, formato, buffn);
      Serial.print("\nPublish message: ");       Serial.println(msg);
      mqtt_sub_pub_client.publish("sensors", msg); // Impacta


    }

  }




  /////////////////////////////////////
  /////////////////////////////////////






  if (currentMillis > millisThingspeak + 15000) ///////////////////////////////////////////////////////////////////// TIMER 3 (c/15 seg)
  {
    millisThingspeak = currentMillis;

#ifdef VENTBOX
    mqttRead(); //Función que realiza una lectura en el Servicor MQTT
#endif


#ifdef ESTACA
    enviaThingspeak(); //Función que realiza una lectura en el Servicor MQTT
#endif

  }



  ////////////////////////////////////////////////////////////////////////////////////////////////////////////// SISTEMA CRÍTICO, SE DISPARA EL RELÉ CUANDO LA TEMPERATURA SEA MAYOR A 30 GRADOS(MAX_TEMP) CELSIUS
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////// SISTEMA CRÍTICO, SE DISPARA EL RELÉ CUANDO LA TEMPERATURA SEA MAYOR A 30 GRADOS(MAX_TEMP) CELSIUS
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////// SISTEMA CRÍTICO, SE DISPARA EL RELÉ CUANDO LA TEMPERATURA SEA MAYOR A 30 GRADOS(MAX_TEMP) CELSIUS


  static boolean estadoLed;
  static unsigned long timer4 = 0;
  if (currentMillis > timer4 + LED_DELAY) //////////////////////TIMER PARPADEO LED SEGÚN TEMPERATURA////////////////// TIMER 4 LED
  {

    //Serial.print("Delay LED" + String (LED_DELAY) + ", ");

    int temp2AUXILIAR = temp2; //18b20

    if (temp2AUXILIAR > LED_PARPADEO_TEMP_MAX) {
      temp2AUXILIAR = LED_PARPADEO_TEMP_MAX;
      enviaAlerta("**ALARMA TEMPERATURA MUY ALTA**");
    }
    if (temp2 < LED_PARPADEO_TEMP_MIN) {
      temp2AUXILIAR = LED_PARPADEO_TEMP_MIN;
      enviaAlerta("**ALARMA TEMPERATURA MUY BAJA**");
    }

    //Controlar el parpadeo del LED
    LED_DELAY = map (temp2AUXILIAR, LED_PARPADEO_TEMP_MIN, LED_PARPADEO_TEMP_MAX, LED_PARPADEO_MAX, LED_PARPADEO_MIN);
    //Regla de 3
    // x=map (temp18B20, entradaMin-temp, entradaMax-temp, salidaMin-tiempo, salidaMax-tiempo)
    timer4 = currentMillis;

    if (estadoLed == 1) estadoLed = 0;
    else
      estadoLed = 1;

    //Serial.print("estadoLed");    Serial.print(estadoLed);    Serial.println("estadoLed");
    digitalWrite(LED_BUILTIN, estadoLed);
  }



  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ACTIVACIÓN PARA QUE NO SOBRECALIENTE LA TEMPERATURA AMBIENTE DEL CULTIVO

  ///////////////////////////////////////// TIMER 5: Comprueba que la temperatura local o la temperatura en la estaca no sea mayor a cierto límite MAX_TEMP
  // Ejemplo de procedimiento para encendido de emergencia de AA
  static unsigned long timer5;
  if (currentMillis > timer5 + 2000)
  {
    timer5 = currentMillis;
    //Sucede cada dos segundos

    /////////////////////////////////////////////////////////////////////////////////////////////////////////// TIMER 5 ACTIVA / DESACTIVA RELAY EN CASO DE EMERGENCIA
    if (temp2 > MAX_TEMP) {
      //AHORA ES CUANDO ACTIVO EL RELÉ
      Serial.println("WARNING!!!! TEMPERATURA LOCAL SUPERIOR A " + String(MAX_TEMP));
      digitalWrite(rele_one, HIGH);   // n the LED on (HIGH is the voltage level)
    }
    else  if (ONLINE_TEMP_CONTROL > MAX_TEMP) {
      //AHORA ES CUANDO ACTIVO EL RELÉ
      Serial.println("WARNING!!!! TEMPERATURA EN ESTACA SUPERIOR A " + String(MAX_TEMP));
      digitalWrite(rele_one, HIGH);   // n the LED on (HIGH is the voltage level)
    } else {


      //AHORA ES CUANDO SE DESACTIVA EL RELÉ
      //Serial.print("Ahora se desactiva el relé.... ");
      digitalWrite(rele_one, LOW);    // turn the LED off by making the voltage LOW

    }
  }




  /////////////////////////////////////////////////// TIMER // Marquesina LCD
  static unsigned long ultimoIngreso;
  if ( millis() > 250 + ultimoIngreso)
  {
    ultimoIngreso = millis();
    marquesinaStatus(lcd_linea1, lcd_linea2);
  }


}



void enviaAlerta(String mensajeAImpactar)
{
  lcd_linea2 += mensajeAImpactar;
  myStatus = lcd_linea2;
  Serial.println(mensajeAImpactar);
  //myStatus es una variable que guarda cadena (String) con mensajes de estado para enviar al Servidor MQTT
}


int suma (int valor1, int valor2)
{
  int r;
  r = valor1 + valor2;
  return r;
}



// function to print a device address
void printAddress(DeviceAddress deviceAddress)
{
  for (uint8_t i = 0; i < 8; i++)
  {
    if (deviceAddress[i] < 16) Serial.print("0");
    Serial.print(deviceAddress[i], HEX);
  }
}





// function to print the temperature for a device
float calcularTemperature(DeviceAddress deviceAddress)
{
  static boolean avisoError18B20;

  float tempC;

  if (!avisoError18B20) {
    sensors.requestTemperatures(); // Send the command to get temperatures
    tempC = sensors.getTempC(deviceAddress);
  }

  if (tempC == DEVICE_DISCONNECTED_C)
  {
    Serial.println("\n***************************************");
    Serial.println("Error: Could not read temperature data");
    Serial.println("***************************************");
    delay (2000);

    avisoError18B20 = true;

    return -255;
  }

  return tempC;

}






















void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, pass);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}


void reconnect_mqtt(int maxTry, int delayMosquitto) {
  int mqttMosquittoIntentos = 0;
  // Loop until we're reconnected
  while (!mqtt_sub_pub_client.connected()      &&  maxTry - mqttMosquittoIntentos    > 0        ) {
    mqttMosquittoIntentos++;
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (mqtt_sub_pub_client.connect(clientId.c_str())) {
      Serial.println("connected");
    } else {
      Serial.print("failed, rc=");
      Serial.print(mqtt_sub_pub_client.state());
      Serial.print(" try again in " + String (delayMosquitto / 1000) + " seconds, remaining=");
      Serial.println(maxTry - mqttMosquittoIntentos);
      delay(delayMosquitto);
    }
  }


  if ( maxTry - mqttMosquittoIntentos    <= 0)
  {
    enviaAlerta("No se pudo conectar a mosquitto");
  }
  else
  {
    enviaAlerta("Mosquitto online");
  }

}


void initDallas()
{
  Serial.println("INICIO initDallas ------------------------------");

  // locate devices on the bus
  Serial.print("Locating devices...");
  sensors.begin();

  //Imprime la cantidad de dispositivos conectados
  Serial.print("Found ");
  Serial.print(sensors.getDeviceCount(), DEC);
  Serial.println(" devices.");

  // report parasite power requirements
  Serial.print("Parasite power is: ");
  if (sensors.isParasitePowerMode()) Serial.println("encendido");
  else Serial.println("apagado");

  if (!sensors.getAddress(insideThermometer, 0)) Serial.println("Unable to find address for Device 0");

  // show the addresses we found on the bus
  Serial.print("Device 0 Address: ");
  printAddress(insideThermometer);
  Serial.println();

  // set the resolution to 9 bit (Each Dallas/Maxim device is capable of several different resolutions)
  sensors.setResolution(insideThermometer, 9);

  Serial.print("Device 0 Resolution: ");
  Serial.print(sensors.getResolution(insideThermometer), DEC);

  Serial.println("  FIN initDallas ------------------------------");

}


void enviaThingspeak()
{
  int x;

  Serial.println("Enviando a Thingspeak");
  ThingSpeak.setField(1, hdty1);   // set the fields with the values
  ThingSpeak.setField(2, temp1);
  ThingSpeak.setField(3, valorLdr);
  ThingSpeak.setField(4, temp2);

  if (myStatus != "" ) ThingSpeak.setStatus(myStatus);   // set the status

  x = ThingSpeak.writeFields(myChannelNumber, myWriteAPIKey);     // write to the ThingSpeak channel
  if (x == 200) {
    Serial.println("Channel update successful.");
    myStatus = "";
  }
  else {
    Serial.println("Problem updating channel. HTTP error code " + String(x));
  }


}


void Bienvenida() {

#ifdef VENTBOX //Lcd solo funciona en modo VENTBOX
  lcd.init();  // initialize the lcd
  lcd.backlight();
  lcd.setCursor(1, 0);
  lcd.print(" TFG de Mikel ");
  lcd.setCursor(0, 1);
  lcd.print("ESTACA MEDICIONHOLA HOA HOLA");
#endif

}


void marquesinaStatus(String linea1, String linea2) {
  static boolean inicioMarquesina = false; //global vs static
  if ( millis() > 2000) { // La función es operativa 2 segundos después de resetearse

#ifdef VENTBOX //Lcd solo funciona en modo VENTBOX
    lcd.scrollDisplayLeft();
    if (inicioMarquesina == false) lcd.clear();
    inicioMarquesina = true;
    lcd.setCursor(0, 0);
    lcd.print(linea1);
    lcd.setCursor(0, 1);
    lcd.print(linea2);
#endif

  }
}


void mqttRead()
{
  int statusCode = 0;
  Serial.print("*******************************************************************");

  // Read and store all the latest field values, location coordinates, status message, and created-at timestamp
  // use ThingSpeak.readMultipleFields(channelNumber, readAPIKey) for private channels
  statusCode = ThingSpeak.readMultipleFields(myChannelNumber);

  if (statusCode == 200)
  {
    // Fetch the stored data

    int online_hdty = ThingSpeak.getFieldAsInt(1); // Field 1
    int online_temp1 = ThingSpeak.getFieldAsInt(2); // Field 2
    int online_lumin = ThingSpeak.getFieldAsInt(3); // Field 3
    int online_temp2 = ThingSpeak.getFieldAsInt(4); // Field 4

    ONLINE_TEMP_CONTROL = online_temp2;

    String statusMessage = ThingSpeak.getStatus(); // Status message
    String latitude = ThingSpeak.getLatitude(); // Latitude
    String longitude = ThingSpeak.getLongitude(); // Longitude
    String elevation = ThingSpeak.getElevation(); // Elevation
    String createdAt = ThingSpeak.getCreatedAt(); // Created-at timestamp

    ////////////////////////////////////////////////////////////////////////////////////////////////////////// imprime lcd

    lcd_linea1 = "T1:" + String (online_temp1) + " T2:" + String(online_temp2)  + " HUM:" + String(online_hdty) + " LUM" + String(online_lumin) ;

    Serial.print("Mensaje LCD Linea 1***" + lcd_linea1 + "******");

    Serial.println();
  }

  else {
    String mensajeError = "Problem reading channel. HTTP error code " + String(statusCode);
    Serial.println(mensajeError);
    lcd_linea1 += mensajeError;
  }
}
